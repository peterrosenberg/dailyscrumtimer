package ch.pete.dailyscrumtimer

interface DailyScrumTimerView {
    fun updateDisplayTime(remainingSeconds: Long)
    fun vibrate(millis: Int)
    fun showSummary(totalTime: Long, teamMembers: Long)
    fun timerStarted()
    fun timerStopped()
    fun showToast(messageRes: Int)
    fun showPreferences()
    fun finish()
}
