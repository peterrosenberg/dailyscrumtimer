package ch.pete.dailyscrumtimer

import android.app.Application
import android.content.SharedPreferences
import android.os.Handler
import androidx.lifecycle.AndroidViewModel

class DailyScrumTimerViewModel(application: Application) : AndroidViewModel(application) {
    /** view of this model */
    private lateinit var view: DailyScrumTimerView
    /** Used to execute the timer every second.  */
    private lateinit var intervalHandler: Handler
    private lateinit var prefs: SharedPreferences
    /** Executed every second to update the time.  */
    private val intervalTimer = IntervalTimer()
    /** The time in seconds until the timer expires.  */

    private var remainingSeconds: Long = 0
    /** Instance of the helper class SoundGenerator to output the peep sound.  */
    private lateinit var soundGenerator: SoundGenerator
    /** State of the timer.  */
    private var timerState = TIMER_STATE_BEFORE_FIRST_START

    /** The amount of seconds where the timer starts to count down.  */
    private var maxSeconds: Int = 0
    /** The amount of seconds left when the first beep is done.  */
    private var firstBeep: Int = 0
    /** The amount of seconds left when the slow beep starts.  */
    private var slowBeep: Int = 0
    /** The amount of seconds left when the fast beep starts.  */
    private var fastBeep: Int = 0
    /** The duration of the last beep in seconds.  */
    private var durationLastBeep: Int = 0

    // statistics
    /** Total time of this Daily Scrum.  */
    private var totalTime = 0L
    /** The amount of team members speaking at this Daily Scrum.  */
    private var teamMembersCount = -1

    // ---------------------------------------------------------------------------------------------
    // View lifecycle methods
    // ---------------------------------------------------------------------------------------------
    fun init(view: DailyScrumTimerView, soundGenerator: SoundGenerator, intervalHandler: Handler, prefs: SharedPreferences) {
        this.view = view
        this.soundGenerator = soundGenerator
        this.intervalHandler = intervalHandler
        this.prefs = prefs
    }

    fun start() {
        getPrefs()
    }

    fun onWindowFocusChanged(hasFocus: Boolean) {
        if (timerState != TIMER_STATE_BEFORE_FIRST_START) {
            if (hasFocus) {
                if (remainingSeconds > 0) {
                    startTimer()
                }
            } else {
                stopTimer()
            }
        }
    }

    override fun onCleared() {
        soundGenerator.release()
        super.onCleared()
    }

    // ---------------------------------------------------------------------------------------------
    // View callbacks
    // ---------------------------------------------------------------------------------------------
    /**
     * Executed then the button "next" is clicked.
     */
    fun nextClick() {
        if (timerState == TIMER_STATE_STARTED) {
            totalTime += (maxSeconds - remainingSeconds).toInt()
        }
        teamMembersCount++
        synchronized(this) {
            soundGenerator.stop()
            remainingSeconds = maxSeconds.toLong()
        }
        startTimer()
    }

    /**
     * Executed then the button "Scrum finished" is clicked.
     */
    fun finishedClick() {
        // calculate the values but do not update the member variables
        // to allow the timer to continue.
        var currentTotalTime = totalTime
        if (timerState == TIMER_STATE_STARTED) {
            currentTotalTime += maxSeconds - remainingSeconds
        }
        val teamMembers = (teamMembersCount + 1).toLong()
        view.showSummary(currentTotalTime, teamMembers)
        stopTimer()
    }

    fun summaryContinueClicked() {
        // check if last person talked too long
        if (remainingSeconds == 0L) {
            // simulate next click
            nextClick()
        } else {
            startTimer()
        }
    }

    fun summaryResetClicked() {
        resetDailyScrum()
    }

    fun summaryFinishClicked() {
        view.finish()
    }

    fun preferencesMenuClicked() {
        view.showPreferences()
    }

    fun returnedFromPreferences() {
        getPrefs()
    }

    // ---------------------------------------------------------------------------------------------
    // Helper methods
    // ---------------------------------------------------------------------------------------------
    /**
     * Activate the timer.
     */
    private fun startTimer() {
        if (timerState != TIMER_STATE_STARTED) {
            timerState = TIMER_STATE_STARTED
            view.timerStarted()
            intervalHandler.removeCallbacks(intervalTimer)
            intervalHandler.postDelayed(intervalTimer, ONE_SECOND.toLong())
        }
        view.updateDisplayTime(remainingSeconds)
    }

    /**
     * Deactivate the timer.
     */
    private fun stopTimer() {
        if (timerState == TIMER_STATE_STARTED) {
            timerState = TIMER_STATE_STOPPED
            intervalHandler.removeCallbacks(intervalTimer)
            view.timerStopped()
            view.updateDisplayTime(remainingSeconds)
        }
    }

    private fun resetDailyScrum() {
        stopTimer()
        timerState = TIMER_STATE_BEFORE_FIRST_START
        remainingSeconds = 0
        teamMembersCount = -1
        totalTime = 0
        view.timerStopped()
        view.updateDisplayTime(remainingSeconds)
    }

    /**
     * Inner class to run the interval timer every second.
     */
    private inner class IntervalTimer : Runnable {
        override fun run() {
            if (remainingSeconds > 0) {
                remainingSeconds--
            }
            val remainingSecondsLocal = remainingSeconds
            view.updateDisplayTime(remainingSeconds)

            if (remainingSecondsLocal > 0) {
                if (remainingSecondsLocal <= fastBeep) {
                    soundGenerator.playSound(FAST_BEEP_TIME)
                } else if (remainingSecondsLocal <= slowBeep && remainingSecondsLocal % 2 == 0L) {
                    soundGenerator.playSound(SLOW_BEEP_TIME)
                } else if (remainingSecondsLocal == firstBeep.toLong()) {
                    soundGenerator.playSound(FIRST_BEEP_TIME)
                }
                intervalHandler.removeCallbacks(this)
                intervalHandler.postDelayed(this, ONE_SECOND.toLong())
            } else {
                view.vibrate(durationLastBeep * ONE_SECOND)
                soundGenerator.playSound(durationLastBeep * ONE_SECOND)

                // use maximum time for this member
                totalTime += (maxSeconds - remainingSeconds).toInt()
                timerState = TIMER_STATE_STOPPED
                intervalHandler.removeCallbacks(intervalTimer)
            }
        }
    }

    // ---------------------------------------------------------------------------------------------
    // preferences
    // ---------------------------------------------------------------------------------------------
    /**
     * Fetches the preferences and update the member variables
     * from the xml/preferences.xml preferences
     */
    private fun getPrefs() {
        val editor = prefs.edit()

        val increaseVolume = prefs.getBoolean(PREFS_INCREASE_VOLUME, false)
        soundGenerator.increaseVolume = increaseVolume

        try {
            maxSeconds = parseIntFromString(prefs.getString(PREFS_TIMER_START, MAX_SECONDS_DEFAULT.toString()))
            if (maxSeconds <= 0) {
                throw NumberFormatException()
            }
        } catch (e: NumberFormatException) {
            maxSeconds = MAX_SECONDS_DEFAULT
            editor.putString(PREFS_TIMER_START, maxSeconds.toString())
        }

        try {
            firstBeep = parseIntFromString(prefs.getString(PREFS_TIMER_FIRST_BEEP, FIRST_BEEP_DEFAULT.toString()))
        } catch (e: NumberFormatException) {
            firstBeep = FIRST_BEEP_DEFAULT
            editor.putString(PREFS_TIMER_FIRST_BEEP, firstBeep.toString())
        }

        try {
            slowBeep = parseIntFromString(prefs
                    .getString(PREFS_TIMER_SLOW_BEEP, SLOW_BEEP_DEFAULT.toString()))
        } catch (e: NumberFormatException) {
            slowBeep = SLOW_BEEP_DEFAULT
            editor.putString(PREFS_TIMER_SLOW_BEEP, slowBeep.toString())
        }

        try {
            fastBeep = parseIntFromString(prefs
                    .getString(PREFS_TIMER_FAST_BEEP, FAST_BEEP_DEFAULT.toString()))
        } catch (e: NumberFormatException) {
            fastBeep = FAST_BEEP_DEFAULT
            editor.putString(PREFS_TIMER_FAST_BEEP, fastBeep.toString())
        }

        try {
            durationLastBeep = parseIntFromString(prefs
                    .getString(PREFS_DURATION_LAST_BEEP, DURATION_LAST_BEEP_DEFAULT.toString()))
        } catch (e: NumberFormatException) {
            durationLastBeep = DURATION_LAST_BEEP_DEFAULT
            editor.putString(PREFS_DURATION_LAST_BEEP, durationLastBeep.toString())
        }

        editor.apply()
    }

    private fun parseIntFromString(value: String?): Int {
        if (value != null) {
            return Integer.parseInt(value)
        } else {
            throw NumberFormatException("value is null")
        }
    }

    companion object {
        /** One second in milli seconds.  */
        private const val ONE_SECOND = 1000

        /** The timer state in the beginning before the first start.  */
        private const val TIMER_STATE_BEFORE_FIRST_START = 0
        /** The timer is running.  */
        private const val TIMER_STATE_STARTED = 1
        /** The timer is stopped.  */
        private const val TIMER_STATE_STOPPED = 2

        /** Default value of maxSeconds.  */
        private const val MAX_SECONDS_DEFAULT = 120
        /** Default value of firstBeep.  */
        private const val FIRST_BEEP_DEFAULT = 30
        /** Default value of slowBeep.  */
        private const val SLOW_BEEP_DEFAULT = 20
        /** Default value of fastBeep.  */
        private const val FAST_BEEP_DEFAULT = 10
        /** Default value of durationLastBeep.  */
        private const val DURATION_LAST_BEEP_DEFAULT = 10

        /** The length of the first beep.  */
        private const val FIRST_BEEP_TIME = 500
        /** The length of the slow beeps.  */
        private const val SLOW_BEEP_TIME = 100
        /** The length of the fast beeps.  */
        private const val FAST_BEEP_TIME = 100

        // preferences constants
        private const val PREFS_INCREASE_VOLUME = "increase_volume"
        private const val PREFS_TIMER_START = "timer_start"
        private const val PREFS_TIMER_FIRST_BEEP = "timer_first_beep"
        private const val PREFS_TIMER_SLOW_BEEP = "timer_slow_beep"
        private const val PREFS_TIMER_FAST_BEEP = "timer_fast_beep"
        private const val PREFS_DURATION_LAST_BEEP = "duration_last_beep"
    }
}
