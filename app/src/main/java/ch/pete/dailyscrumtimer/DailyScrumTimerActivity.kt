/*
 * Copyright (C) 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.pete.dailyscrumtimer

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.media.AudioManager
import android.media.ToneGenerator
import android.os.Bundle
import android.os.Handler
import android.os.Vibrator
import android.preference.PreferenceManager
import android.view.Menu
import android.view.MenuItem
import android.view.WindowManager
import android.widget.Button
import android.widget.Toast
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.ViewModelProviders
import kotlinx.android.synthetic.main.main.*
import java.text.SimpleDateFormat
import java.util.*

/**
 * Main activity of Daily Scrum Timer.
 */
class DailyScrumTimerActivity : FragmentActivity(), DailyScrumTimerView {
    private lateinit var viewModel: DailyScrumTimerViewModel

    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main)

        viewModel = ViewModelProviders.of(this).get(DailyScrumTimerViewModel::class.java)

        // use full screen and ensure, the screen is not switched of while the
        // timer is visible.
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN)
        window.setFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED,
                WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED)
        window.setFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD,
                WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD)

        // Capture our button from layout
        buttonNext.setOnClickListener { viewModel.nextClick() }
        buttonFinished.visibility = Button.INVISIBLE
        buttonFinished.setOnClickListener { viewModel.finishedClick() }

        val toneGenerator = ToneGenerator(AudioManager.STREAM_DTMF, ToneGenerator.MAX_VOLUME)
        val soundGenerator = SoundGenerator(application, false, toneGenerator)
        val prefs = PreferenceManager.getDefaultSharedPreferences(this)
        viewModel.init(this, soundGenerator, Handler(), prefs)
    }

    override fun onStart() {
        super.onStart()
        viewModel.start()
    }

    override fun onWindowFocusChanged(hasFocus: Boolean) {
        viewModel.onWindowFocusChanged(hasFocus)
        super.onWindowFocusChanged(hasFocus)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            REQUEST_CODE_PREFERENCES -> viewModel.returnedFromPreferences()
            else -> super.onActivityResult(requestCode, resultCode, data)
        }
    }

    // ---------------------------------------------------------------------------------------------
    // menu
    // ---------------------------------------------------------------------------------------------
    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.preferences -> {
                viewModel.preferencesMenuClicked()
            }
        }// do nothing
        return true
    }

    override fun timerStarted() {
        buttonFinished.visibility = Button.VISIBLE
        window.setFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON,
                WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
    }

    override fun timerStopped() {
        buttonFinished.visibility = Button.INVISIBLE
        window.clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
    }

    /**
     * Shows the summary dialog box.
     */
    override fun showSummary(totalTime: Long, teamMembers: Long) {
        val sdf = SimpleDateFormat(FORMAT_MINUTE_SECOND, Locale.US)
        sdf.timeZone = TimeZone.getTimeZone(TIME_ZONE_GMT)

        val averageTime = if (teamMembers > 0) totalTime / teamMembers else 0
        val averageTimeStr = sdf.format(Date(averageTime * CONVERT_TO_SECONDS))
        val totalTimeStr = sdf.format(Date(totalTime * CONVERT_TO_SECONDS))
        val message = String.format(getString(R.string.summary), teamMembers, averageTimeStr, totalTimeStr)

        val builder = AlertDialog.Builder(this)
        builder.setMessage(message).setCancelable(true)
                .setPositiveButton(R.string.continue_btn) { _, _ ->
                    viewModel.summaryContinueClicked()
                }
                .setNeutralButton(R.string.reset_btn) { _, _ ->
                    viewModel.summaryResetClicked()
                }
                .setNegativeButton(R.string.finish_btn) { _, _ ->
                    viewModel.summaryFinishClicked()
                }
        builder.create().show()
    }

    /**
     * Update the time on the display.
     */
    override fun updateDisplayTime(remainingSeconds: Long) {
        val sdf = SimpleDateFormat(FORMAT_MINUTE_SECOND, Locale.US)
        sdf.timeZone = TimeZone.getTimeZone(TIME_ZONE_GMT)
        timerOutput.text = sdf.format(Date(remainingSeconds * CONVERT_TO_SECONDS))
    }

    /**
     * Vibrate if allowed.
     *
     * @param millis milli seconds to vibrate
     */
    override fun vibrate(millis: Int) {
        val vibrator = getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
        try {
            vibrator.vibrate(millis.toLong()) // else ignore vibration
        } catch (e: SecurityException) {
            // permission denied, ignore vibration
        }
    }

    override fun showToast(messageRes: Int) {
        Toast.makeText(this, messageRes, Toast.LENGTH_LONG).show()
    }

    override fun showPreferences() {
        val settingsActivity = Intent(baseContext, Preferences::class.java)
        startActivityForResult(settingsActivity, REQUEST_CODE_PREFERENCES)
    }

    companion object {
        /** Used to covert the value to seconds.  */
        private const val CONVERT_TO_SECONDS: Long = 1000

        private const val FORMAT_MINUTE_SECOND = "mm:ss"
        private const val TIME_ZONE_GMT = "GMT+0"

        private const val REQUEST_CODE_PREFERENCES = 1
    }
}
