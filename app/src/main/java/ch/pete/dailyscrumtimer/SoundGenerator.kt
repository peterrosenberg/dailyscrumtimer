/*
 * Copyright (C) 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.pete.dailyscrumtimer

import android.content.Context
import android.media.AudioManager
import android.media.ToneGenerator
import java.util.concurrent.Executors
import java.util.concurrent.ScheduledExecutorService
import java.util.concurrent.TimeUnit

/**
 * Helper class to play sounds.
 */
class SoundGenerator(context: Context, var increaseVolume: Boolean, private val toneGenerator: ToneGenerator) {

    /** The AudioManager of the system, null if volume not increased.  */
    private val audioManager: AudioManager?

    /** The task executor to set the volume back, null if volume not increased.  */
    private val scheduleTaskExecutor: ScheduledExecutorService?

    init {
        if (increaseVolume) {
            audioManager = context.getSystemService(Context.AUDIO_SERVICE) as AudioManager
            scheduleTaskExecutor = Executors.newScheduledThreadPool(1)
        } else {
            audioManager = null
            scheduleTaskExecutor = null
        }
    }

    /**
     * Stops the playback immediately.
     */
    @Synchronized
    fun stop() {
        toneGenerator.stopTone()
    }

    @Synchronized
    fun release() {
        toneGenerator.release()
    }

    /**
     * Plays the created sound.
     *
     * @param durationMs the sound is played in milli seconds.
     * @return true if sound played, false if permission was denied
     */
    @Synchronized
    fun playSound(durationMs: Int): Boolean {
        try {
            val streamVolume: Int
            if (audioManager != null) {
                streamVolume = audioManager.getStreamVolume(AudioManager.STREAM_DTMF)
                audioManager.setStreamVolume(AudioManager.STREAM_DTMF,
                        audioManager.getStreamMaxVolume(AudioManager.STREAM_DTMF), 0)
            } else {
                streamVolume = 0
            }
            toneGenerator.startTone(ToneGenerator.TONE_CDMA_DIAL_TONE_LITE, durationMs)

            scheduleTaskExecutor?.schedule(
                    {
                        audioManager?.setStreamVolume(AudioManager.STREAM_DTMF, streamVolume, 0)
                    },
                    durationMs.toLong(),
                    TimeUnit.MILLISECONDS
            )
            return true
        } catch (e: SecurityException) {
            e.printStackTrace()
            return false
        }
    }
}
