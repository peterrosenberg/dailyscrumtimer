package ch.pete.dailyscrumtimer

import android.app.Application
import android.content.SharedPreferences
import android.os.Handler
import com.nhaarman.mockito_kotlin.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.junit.MockitoJUnit
import org.mockito.junit.MockitoRule

class DailyScrumTimerViewModelTest {
    @get:Rule
    val mockitoRule: MockitoRule = MockitoJUnit.rule()

    @Mock
    private lateinit var application: Application
    @Mock
    private lateinit var view: DailyScrumTimerView
    @Mock
    private lateinit var soundGenerator: SoundGenerator
    @Mock
    private lateinit var intervalHandler: Handler
    @Mock
    private lateinit var prefs: SharedPreferences
    @Mock
    private lateinit var prefsEditor: SharedPreferences.Editor

    private lateinit var intervalTimer: Runnable

    private lateinit var model: DailyScrumTimerViewModel

    @Before
    fun setUp() {
        whenever(prefs.edit()).thenReturn(prefsEditor)
        whenever(prefs.getString(eq("timer_start"), any())).thenReturn("120")

        whenever(intervalHandler.postDelayed(any(), any())).thenAnswer { invocationOnMock ->
            intervalTimer = invocationOnMock.arguments[0] as Runnable
            true
        }

        model = DailyScrumTimerViewModel(application)
        model.init(view, soundGenerator, intervalHandler, prefs)
        model.start()
    }

    @Test
    fun start() {
        verify(prefs, atLeastOnce()).getString(any(), any())
        verify(prefsEditor).apply()
    }

    @Test
    fun onWindowFocusChanged_when_hasFocus() {
        model.nextClick()
        model.onWindowFocusChanged(false)
        reset(view)

        model.onWindowFocusChanged(true)

        verify(view, never()).timerStopped()
        verify(view).timerStarted()
        verify(view).updateDisplayTime(any())
    }


    @Test
    fun onWindowFocusChanged_when_hasFocusAndCountdownReached() {
        whenever(prefs.getString(eq("timer_start"), any())).thenReturn("1")
        model.start()
        model.nextClick()
        intervalTimer.run()
        model.onWindowFocusChanged(false)
        reset(view)

        model.onWindowFocusChanged(true)

        // one from nextClick()
        verify(view, never()).timerStarted()
        verify(view, never()).timerStopped()
        verify(view, never()).updateDisplayTime(any())
    }

    @Test
    fun onWindowFocusChanged_when_hasNoFocus() {
        model.nextClick()

        model.onWindowFocusChanged(false)

        // one from nextClick()
        verify(view).timerStarted()
        verify(view).timerStopped()
    }

    @Test
    fun nextClick_when_timerNotStarted() {
        model.nextClick()

        verify(view).timerStarted()
        verify(view).updateDisplayTime(120)
    }

    @Test
    fun nextClick_when_timerStarted() {
        model.nextClick()

        model.nextClick()

        verify(view).timerStarted()
        verify(view, times(2)).updateDisplayTime(120)
    }

    @Test
    fun finishedClick_when_timerStarted() {
        model.nextClick()
        model.finishedClick()

        verify(view).timerStopped()
        // one from nextClick()
        verify(view, times(2)).updateDisplayTime(120)
        verify(view).showSummary(0, 1)
    }

    @Test
    fun continueClicked() {
        model.nextClick()

        model.summaryContinueClicked()

        // one from nextClick()
        verify(view, times(2)).updateDisplayTime(120)
    }

    @Test
    fun finishClicked() {
        model.summaryFinishClicked()

        verify(view).finish()
    }

    @Test
    fun countdownReached() {
        whenever(prefs.getString(eq("timer_start"), any())).thenReturn("1")
        model.start()

        model.nextClick()
        verify(view).updateDisplayTime(1)

        intervalTimer.run()
        verify(view).updateDisplayTime(0)

        model.finishedClick()
        verify(view).showSummary(1, 1)

        model.nextClick()
        verify(view, times(2)).updateDisplayTime(1)

        model.finishedClick()
        verify(view).showSummary(1, 2)
    }

    @Test
    fun preferencesMenuClicked() {
        model.preferencesMenuClicked()

        verify(view).showPreferences()
    }
}
